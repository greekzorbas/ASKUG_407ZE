/**
  ******************************************************************************
  * File Name          : rs232.c
  * Description        : This file provides code for RS232 operations.
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rs232.h"

/* Global variables ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static CCMRAM uint8_t rxBuf[RS232_BUF_LEN];
static CCMRAM uint8_t txBuf[RS232_BUF_LEN];
static uint8_t rxBufPrtIn = 0;
static uint8_t rxBufPrtOut = 0;
static uint8_t txBufPrtIn = 0;
static uint8_t txBufPrtOut = 0;

static SemaphoreHandle_t xRs232RxIRQ = NULL;
static SemaphoreHandle_t xRs232DataReady = NULL;
static TaskHandle_t xRs232ReceiveTask = NULL;
static TaskHandle_t xRs232DataReadyTask = NULL;

/* Private function prototypes -----------------------------------------------*/
static void xRs232ReceiveHandle(void *pvParameters);
static void xRs232DataReadyHandle(void *pvParameters);





/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/**
  * @brief  Handles receiving data via RS232 then store the data to a buffer 
  * @param  item: RS485_TypeDef pointer
  * @return None
  */
void RS232_Handler() {
  uint8_t tmp = USART1->DR;
  uint8_t preRxPtr = rxBufPrtIn - 1;
  preRxPtr &= RS232_BUF_MASK;

  rxBuf[(rxBufPrtIn++)] = tmp;
  rxBufPrtIn &= RS232_BUF_MASK;

  // the end of message
  // if ((rxBuf[preRxPtr] == 0x0d) && (tmp == 0x0a)) {
  //   if (tmp == '\n') { // 0x0a
  //     if (rxBuf[preRxPtr] == '\r') { // 0x0d
  //       // ToDo Handle the end of message
  //     }
  //   }
  // }

  // linux screen sends only CR, to work with devices NL CR LF are needed,
  // a CR immediately followed by a LF (CRLF, \r\n, or 0x0d0a) 
  if (tmp == '\r') { // 0x0d
    xSemaphoreGive(xRs232DataReady);
  }
}


/**
  * @brief  Reads RS232 buffer data to another buffer 
  * @param  buf: pointer to a buffer where to store the data 
  * @param  len: length of bytes to read
  * @return (int) payload length of the data 
  */
uint8_t RS232_RxBufferRead(uint8_t *buf, uint16_t len) {
  uint8_t payloadLen = 0;
  while (rxBufPrtOut != rxBufPrtIn) {
    
    buf[(payloadLen++)] = rxBuf[(rxBufPrtOut++)];
    rxBufPrtOut &= RS232_BUF_MASK;
    if (payloadLen >= (len - 1)) break;
  }
  return payloadLen;
}


/**
  * @brief This function handles USART1 global interrupt.
  */
void RS232_IRQHandler(void) {
  static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  PREG_CLR(USART1->SR, USART_SR_RXNE_Pos);
  
  xSemaphoreGiveFromISR(xRs232RxIRQ, &xHigherPriorityTaskWoken);
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);

}


/**
  * @brief  Provides FreeRTOS RS232 receiver service.
  * @return None
  */
void srvRS232Receiver(void) {

  bzero(rxBuf, sizeof(rxBuf));
  bzero(txBuf, sizeof(txBuf));

  xRs232RxIRQ = xSemaphoreCreateBinary();
  xRs232DataReady = xSemaphoreCreateBinary();
  xTaskCreate(xRs232ReceiveHandle, "RS232Receive", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1), &xRs232ReceiveTask);
  xTaskCreate(xRs232DataReadyHandle, "RS232Ready", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1), &xRs232DataReadyTask);
}


/**
  * @brief  Handles RS232 receiver.
  * @param  pvParameters: pointer to FreeRTOS parameters
  * @return None
  */
static void xRs232ReceiveHandle(void *pvParameters) {
  (void)pvParameters;

  // static portBASE_TYPE xHigherPriorityTaskWoken = NULL; 
  while(1) {
    if (xSemaphoreTakeFromISR(xRs232RxIRQ, NULL) == pdTRUE) {
      // printf("Receive Semaphor\n");
      RS232_Handler();
    }
  }
}


/**
  * @brief  Handles received data.
  * @param  pvParameters: pointer to FreeRTOS parameters
  * @return None
  */
static void xRs232DataReadyHandle(void *pvParameters) {
  (void)pvParameters;

  // static portBASE_TYPE xHigherPriorityTaskWoken = NULL; 
  while(1) {
    if (xSemaphoreTake(xRs232DataReady, TIMER_RX_SEM_TO_WAIT) == pdTRUE) {
      uint8_t buf[16];
      bzero(&buf, sizeof(buf));
      RS232_RxBufferRead(buf, sizeof(buf));
      printf("%s\n", buf);
    }
  }
}


/*************************** © Zoo IPS, 2021 **********************************/
