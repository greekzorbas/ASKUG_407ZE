/**
  ******************************************************************************
  * File Name          : rs232.c
  * Description        : This file provides code for RS232 operations.
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rs232.h"

/* Global variables ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
static CCMRAM uint8_t rxBuf_1[RS485_1_BUF_LEN];
static CCMRAM uint8_t txBuf_1[RS485_1_BUF_LEN];
static CCMRAM uint8_t rxBuf_2[RS485_2_BUF_LEN];
static CCMRAM uint8_t txBuf_2[RS485_2_BUF_LEN];

static SemaphoreHandle_t xRs485_1RxIRQ = NULL;
static SemaphoreHandle_t xRs485_1DataReady = NULL;
static TaskHandle_t xRs485_1ReceiveTask = NULL;
static TaskHandle_t xRs485_1DataReadyTask = NULL;

static SemaphoreHandle_t xRs485_2RxIRQ = NULL;
static SemaphoreHandle_t xRs485_2DataReady = NULL;
static TaskHandle_t xRs485_2ReceiveTask = NULL;
static TaskHandle_t xRs485_2DataReadyTask = NULL;



static RS485_TypeDef item_1 = {
  .usart          = USART2,
  .rxBuf          = rxBuf_1,
  .rxBufLen       = RS485_1_BUF_LEN,
  .rxBufMask      = RS485_1_BUF_MASK,
  .rxBufPrtIn     = 0,
  .rxBufPrtOut    = 0,
  .txBuf          = txBuf_1,
  .txBufLen       = RS485_1_BUF_LEN,
  .txBufMask      = RS485_1_BUF_MASK,
  .txBufPrtIn     = 0,
  .txBufPrtOut    = 0,
  .irqSem         = &xRs485_1RxIRQ,
  .dataReadySem   = &xRs485_1DataReady
};

static RS485_TypeDef item_2 = {
  .usart          = USART3,
  .rxBuf          = rxBuf_2,
  .rxBufLen       = RS485_2_BUF_LEN,
  .rxBufMask      = RS485_2_BUF_MASK,
  .rxBufPrtIn     = 0,
  .rxBufPrtOut    = 0,
  .txBuf          = txBuf_2,
  .txBufLen       = RS485_2_BUF_LEN,
  .txBufMask      = RS485_2_BUF_MASK,
  .txBufPrtIn     = 0,
  .txBufPrtOut    = 0,
  .irqSem         = &xRs485_2RxIRQ,
  .dataReadySem   = &xRs485_2DataReady
};

/* Private function prototypes -----------------------------------------------*/
static void xRs485ReceiveHandle(void *pvParameters);
static void xRs485DataReadyHandle(void *pvParameters);






/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/**
  * @brief  Handles receiving data via RS485 then store the data to a buffer 
  * @param  item: RS485_TypeDef pointer
  * @return None
  */
void RS485_ReceiveHandler(RS485_TypeDef* item) {
  uint8_t tmp = item->usart->DR;
  uint8_t preRxPtr = item->rxBufPrtIn - 1;
  preRxPtr &= item->rxBufMask;

  item->rxBuf[(item->rxBufPrtIn++)] = tmp;
  item->rxBufPrtIn &= item->rxBufMask;

  // // the end of message
  // if ((item->rxBuf[preRxPtr] == 0x0d) && (tmp == 0x0a)) {
  //   if (tmp == '\n') { // 0x0a
  //     if (item->rxBuf[preRxPtr] == '\r') { // 0x0d
  //       // ToDo handle the end of message
  //     }
  //   }
  // }

  // linux screen sends only CR, to work with devices NL CR LF are needed,
  // a CR immediately followed by a LF (CRLF, \r\n, or 0x0d0a) 
  if (tmp == '\r') { // 0x0d
    xSemaphoreGive(*item->dataReadySem);
  }

}


/**
  * @brief  Reads RS485 buffer data to another buffer 
  * @param  item: RS485_TypeDef pointer
  * @param  buf: pointer to a buffer where to store the data 
  * @param  len: length of bytes to read
  * @return (int) payload length of the data 
  */
uint8_t RS485_RxBufferRead(RS485_TypeDef* item, uint8_t *buf, uint16_t len) {
  uint8_t payloadLen = 0;
  while (item->rxBufPrtOut != item->rxBufPrtIn) {
    buf[(payloadLen++)] = item->rxBuf[(item->rxBufPrtOut++)];
    item->rxBufPrtOut &= item->rxBufMask;
    if (payloadLen >= (len - 1)) break;
  }
  return payloadLen;
}


/**
  * @brief This function handles USART2 global interrupt.
  */
void RS485_1_IRQHandler(void) {
  static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  PREG_CLR(USART2->SR, USART_SR_RXNE_Pos);

  // xSemaphoreGiveFromISR(xRs485_1RxIRQ, &xHigherPriorityTaskWoken);
  xSemaphoreGiveFromISR(*item_1.irqSem, &xHigherPriorityTaskWoken);
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/**
  * @brief This function handles USART3 global interrupt.
  */
void RS485_2_IRQHandler(void) {
  static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  PREG_CLR(USART3->SR, USART_SR_RXNE_Pos);

  // xSemaphoreGiveFromISR(xRs485_2RxIRQ, &xHigherPriorityTaskWoken);
  xSemaphoreGiveFromISR(*item_2.irqSem, &xHigherPriorityTaskWoken);
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/**
  * @brief  Provides FreeRTOS RS485 receiver service.
  * @return None
  */
void srvRS485Receiver(void) {

  bzero(rxBuf_1, sizeof(rxBuf_1));
  bzero(txBuf_1, sizeof(txBuf_1));
  bzero(rxBuf_2, sizeof(rxBuf_2));
  bzero(txBuf_2, sizeof(txBuf_2));

  *item_1.irqSem = xSemaphoreCreateBinary();
  *item_1.dataReadySem = xSemaphoreCreateBinary();
  xTaskCreate(xRs485ReceiveHandle, "RS485_1Receive", configMINIMAL_STACK_SIZE, &item_1, (tskIDLE_PRIORITY + 1), &xRs485_1ReceiveTask);
  xTaskCreate(xRs485DataReadyHandle, "RS485_1Ready", configMINIMAL_STACK_SIZE, &item_1, (tskIDLE_PRIORITY + 1), &xRs485_1DataReadyTask);

  *item_2.irqSem = xSemaphoreCreateBinary();
  *item_2.dataReadySem = xSemaphoreCreateBinary();
  xTaskCreate(xRs485ReceiveHandle, "RS485_2Receive", configMINIMAL_STACK_SIZE, &item_2, (tskIDLE_PRIORITY + 1), &xRs485_2ReceiveTask);
  xTaskCreate(xRs485DataReadyHandle, "RS485_2Ready", configMINIMAL_STACK_SIZE, &item_2, (tskIDLE_PRIORITY + 1), &xRs485_2DataReadyTask);
}


/**
  * @brief  Handles RS485 receiver.
  * @param  pvParameters: pointer to RS485_TypeDef item
  * @return None
  */
static void xRs485ReceiveHandle(void *pvParameters) {
  RS485_TypeDef* item = pvParameters;

  while(1) {
    if (xSemaphoreTakeFromISR(*item->irqSem, NULL) == pdTRUE) {
      // printf("Receive Semaphor\n");
      RS485_ReceiveHandler(item);
    }
  }
}


/**
  * @brief  Handles RS485 received data.
  * @param  pvParameters: pointer to RS485_TypeDef item
  * @return None
  */
static void xRs485DataReadyHandle(void *pvParameters) {
  RS485_TypeDef* item = pvParameters;

  while(1) {
    if (xSemaphoreTake(*item->dataReadySem, TIMER_RX_SEM_TO_WAIT) == pdTRUE) {
      uint8_t buf[16];
      bzero(&buf, sizeof(buf));
      RS485_RxBufferRead(item, buf, sizeof(buf));
      printf("%s\n", buf);
    }
  }
}


/*************************** © Zoo IPS, 2021 **********************************/
