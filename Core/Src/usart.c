/**
  ******************************************************************************
  * File Name          : usart.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"





/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/**
  * @brief  Initializes USART peripheral 
  * @param  dev: USART_TypeDef pointer
  * @return None
  */
int8_t USART_Init(USART_TypeDef* dev, uint16_t baudrate) {
  int8_t state = -1; 

  if (dev == USART1) {
    /* ###################################################################### */
    /* - - - - 1-st step. Adjust GPIO - - - - */
    /* Define mask */
    #define USART_1_MASK     (USART_1_TX_Pin_Mask | USART_1_RX_Pin_Mask)
    /* Mode alternative */
    MODIFY_REG(USART_1_Port->MODER, USART_1_MASK, (
        (_MODE_AF << USART_1_TX_Pin_Pos * 2)
      | (_MODE_AF << USART_1_RX_Pin_Pos * 2)
    ));
    /* Speed very high */
    MODIFY_REG(USART_1_Port->OSPEEDR, USART_1_MASK, (
        (_SPEED_V << USART_1_TX_Pin_Pos * 2)
      | (_SPEED_V << USART_1_RX_Pin_Pos * 2)
    ));
    /* Output type none */
    /* Push mode none */
    /* Alternate function USART */
    MODIFY_REG(USART_1_Port->AFR[1], 0x00000ff0, (
        (GPIO_AF_7 << ((USART_1_TX_Pin_Pos - 8) * 4))
      | (GPIO_AF_7 << ((USART_1_RX_Pin_Pos - 8) * 4))
    ));


    /* - - - - 2-nd step. Setup USART1 - - - - */
    /* Transmit enable */
    PREG_SET(USART1->CR1, USART_CR1_TE_Pos);
    /* Receive enable */
    PREG_SET(USART1->CR1, USART_CR1_RE_Pos);
    /* Asyncronos mode */
    PREG_CLR(USART1->CR2, USART_CR2_LINEN_Pos);
    PREG_CLR(USART1->CR2, USART_CR2_CLKEN_Pos);
    PREG_CLR(USART1->CR3, USART_CR3_SCEN);
    PREG_CLR(USART1->CR3, USART_CR3_IREN);
    PREG_CLR(USART1->CR3, USART_CR3_HDSEL);
    /* Set Baudrate */
    USART1->BRR = baudrate;
    /* Enable USART1 */
    PREG_SET(USART1->CR1, USART_CR1_UE_Pos);
    /* Enable RXNE Interrupt */
    PREG_SET(USART1->CR1, USART_CR1_RXNEIE_Pos);


    /* - - - - 3-nd step. Adjust interrupt on receive on the USART1 - - - - */
    /* USART1 interrupt Init */
    NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 15, 0));
    NVIC_EnableIRQ(USART1_IRQn);

    state = 0;
  } else if (dev == USART2) {
    /* ###################################################################### */
    /* - - - - 1-st step. Adjust GPIO - - - - */
    /* Define mask */
    #define USART_2_MASK     (USART_2_TX_Pin_Mask | USART_2_RX_Pin_Mask)
    /* Mode alternative */
    MODIFY_REG(USART_2_Port->MODER, USART_2_MASK, (
        (_MODE_AF << USART_2_TX_Pin_Pos * 2)
      | (_MODE_AF << USART_2_RX_Pin_Pos * 2)
    ));
    /* Speed very high */
    MODIFY_REG(USART_2_Port->OSPEEDR, USART_2_MASK, (
        (_SPEED_V << USART_2_TX_Pin_Pos * 2)
      | (_SPEED_V << USART_2_RX_Pin_Pos * 2)
    ));
    /* Output type none */
    /* Push mode none */
    /* Alternate function USART */
    MODIFY_REG(USART_2_Port->AFR[0], 0x0ff00000, (
        (GPIO_AF_7 << (USART_2_TX_Pin_Pos * 4))
      | (GPIO_AF_7 << (USART_2_RX_Pin_Pos * 4))
    ));


    /* - - - - 2-nd step. Setup USART2 - - - - */
    /* Transmit enable */
    PREG_SET(USART2->CR1, USART_CR1_TE_Pos);
    /* Receive enable */
    PREG_SET(USART2->CR1, USART_CR1_RE_Pos);
    /* Asyncronos mode */
    PREG_CLR(USART2->CR2, USART_CR2_LINEN_Pos);
    PREG_CLR(USART2->CR2, USART_CR2_CLKEN_Pos);
    PREG_CLR(USART2->CR3, USART_CR3_SCEN);
    PREG_CLR(USART2->CR3, USART_CR3_IREN);
    PREG_CLR(USART2->CR3, USART_CR3_HDSEL);
    /* Set Baudrate */
    USART2->BRR = baudrate;
    /* Enable USART2 */
    PREG_SET(USART2->CR1, USART_CR1_UE_Pos);
    /* Enable RXNE Interrupt */
    PREG_SET(USART2->CR1, USART_CR1_RXNEIE_Pos);


    /* - - - - 3-nd step. Adjust interrupt on receive on the USART2 - - - - */
    /* USART2 interrupt Init */
    NVIC_SetPriority(USART2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 15, 0));
    NVIC_EnableIRQ(USART2_IRQn);

    /* Set RX485_1 to Receive mode */
    MODIFY_REG(RS485_1_E_Port->MODER, RS485_1_E_Pin_Mask, _MODE_OUT << RS485_1_E_Pin_Pos * 2);
    RS485_1_RX;

    state = 0;
  } else if (dev == USART3) {
    /* ###################################################################### */
    /* - - - - 1-st step. Adjust GPIO - - - - */
    /* Define mask */
    #define USART_3_MASK     (USART_3_TX_Pin_Mask | USART_3_RX_Pin_Mask)
    /* Mode alternative */
    MODIFY_REG(USART_3_Port->MODER, USART_3_MASK, (
        (_MODE_AF << USART_3_TX_Pin_Pos * 2)
      | (_MODE_AF << USART_3_RX_Pin_Pos * 2)
    ));
    /* Speed very high */
    MODIFY_REG(USART_3_Port->OSPEEDR, USART_3_MASK, (
        (_SPEED_V << USART_3_TX_Pin_Pos * 2)
      | (_SPEED_V << USART_3_RX_Pin_Pos * 2)
    ));
    /* Output type none */
    /* Push mode none */
    /* Alternate function USART */
    MODIFY_REG(USART_3_Port->AFR[1], 0x0000ff00, (
        (GPIO_AF_7 << ((USART_3_TX_Pin_Pos - 8) * 4))
      | (GPIO_AF_7 << ((USART_3_RX_Pin_Pos - 8) * 4))
    ));


    /* - - - - 2-nd step. Setup USART3 - - - - */
    /* Transmit enable */
    PREG_SET(USART3->CR1, USART_CR1_TE_Pos);
    /* Receive enable */
    PREG_SET(USART3->CR1, USART_CR1_RE_Pos);
    /* Asyncronos mode */
    PREG_CLR(USART3->CR2, USART_CR2_LINEN_Pos);
    PREG_CLR(USART3->CR2, USART_CR2_CLKEN_Pos);
    PREG_CLR(USART3->CR3, USART_CR3_SCEN);
    PREG_CLR(USART3->CR3, USART_CR3_IREN);
    PREG_CLR(USART3->CR3, USART_CR3_HDSEL);
    /* Set Baudrate */
    USART3->BRR = baudrate;
    /* Enable USART3 */
    PREG_SET(USART3->CR1, USART_CR1_UE_Pos);
    /* Enable RXNE Interrupt */
    PREG_SET(USART3->CR1, USART_CR1_RXNEIE_Pos);


    /* - - - - 3-nd step. Adjust interrupt on receive on the USART3 - - - - */
    /* USART3 interrupt Init */
    NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 15, 0));
    NVIC_EnableIRQ(USART3_IRQn);

    state = 0;
  } 

  /* Set RX485_2 to Receive mode */
  MODIFY_REG(RS485_2_E_Port->MODER, RS485_2_E_Pin_Mask, _MODE_OUT << RS485_2_E_Pin_Pos * 2);
  RS485_2_RX;

  return state;
}


/*************************** © Zoo IPS, 2021 **********************************/
