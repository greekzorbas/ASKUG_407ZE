/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Global variables ---------------------------------------------------------*/
uint32_t SystemCoreClock  = 16000000U;
RCC_ClocksTypeDef RccClocks;

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/







////////////////////////////////////////////////////////////////////////////////

/**
  * @brief  The application entry point.
  * @return int
  */
int main(void) {
  Delay(500);

  /* LED */
  LED_Init();

  // /* EXTI */
  // EXTI_Init();

  /* USART */ 
  USART_Init(USART1, RS232_BRR_Value);
  USART_Init(USART2, RS485_1_BRR_Value);
  USART_Init(USART3, RS485_2_BRR_Value);

  /* RTC */
  RTC_Init();

  // /* Basic Timer7 */
  // BasicTimer7_Init();

  // /* SPI */
  // SPI_Init(SPI2);
  // SPI_Init(SPI3);

  // /* EEPROM */
  // W25qxx_Init();


  /* -------------------------------------- */
  /* ---          Run FreeRTOS          --- */
  /* -------------------------------------- */
  FreeRTOS_Run();

  while (1) {
    // ToDo error handler
  }
}



/**
  * @brief  Reset of all peripherals, Initializes the Flash interface and the Systick.
  * @return None
  */
void SystemInit(void) {

  #if (INSTRUCTION_CACHE_ENABLE != 0U)
    PREG_SET(FLASH->ACR, FLASH_ACR_ICEN_Pos);
  #endif /* INSTRUCTION_CACHE_ENABLE */

  #if (DATA_CACHE_ENABLE != 0U)
    PREG_SET(FLASH->ACR, FLASH_ACR_DCEN_Pos);
  #endif /* DATA_CACHE_ENABLE */

  #if (PREFETCH_ENABLE != 0U)
    PREG_SET(FLASH->ACR, FLASH_ACR_PRFTEN_Pos);
  #endif /* PREFETCH_ENABLE */

  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* SysCfg */
  PREG_SET(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN_Pos);
  while (!(PREG_CHECK(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN_Pos)));

  /* PWR */
  PREG_SET(RCC->APB1ENR, RCC_APB1ENR_PWREN_Pos);
  while (!(PREG_CHECK(RCC->APB1ENR, RCC_APB1ENR_PWREN_Pos)))

  /* FLASH_IRQn interrupt configuration */
  NVIC_SetPriority(FLASH_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_EnableIRQ(FLASH_IRQn);

  /* RCC_IRQn interrupt configuration */
  NVIC_SetPriority(RCC_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_EnableIRQ(RCC_IRQn);


  MODIFY_REG(FLASH->ACR, FLASH_ACR_LATENCY, FLASH_ACR_LATENCY_5WS);

  if (READ_BIT(FLASH->ACR, FLASH_ACR_LATENCY) != FLASH_ACR_LATENCY_5WS) {
    Error_Handler();
  }
  
  MODIFY_REG(PWR->CR, PWR_CR_VOS_Msk, PWR_CR_VOS);

   /* HSE enable and wait until ready */
  PREG_SET(RCC->CR, RCC_CR_HSEON_Pos);
  while(!(PREG_CHECK(RCC->CR, RCC_CR_HSERDY_Pos)));
  
  /* LSI enable and wait until ready */
  PREG_SET(RCC->CSR, RCC_CSR_LSION_Pos);
  while(!(PREG_CHECK(RCC->CSR, RCC_CSR_LSIRDY_Pos)));

  /* Backup registers enable access */
  PREG_SET(PWR->CR, PWR_CR_DBP_Pos);

  /* force backup domain reset */
  PREG_SET(RCC->BDCR, RCC_BDCR_BDRST_Pos);
  PREG_CLR(RCC->BDCR, RCC_BDCR_BDRST_Pos);
  
  /* LSE enable and wait until ready */
  PREG_SET(RCC->BDCR, RCC_BDCR_LSEON_Pos);
  while(!(PREG_CHECK(RCC->BDCR, RCC_BDCR_LSERDY_Pos)));

  /* RTC source is LSE */
  MODIFY_REG(RCC->BDCR, RCC_BDCR_RTCSEL_Msk, RCC_BDCR_RTCSEL_0);

  /* Enable RTC */
  PREG_SET(RCC->BDCR, RCC_BDCR_RTCEN_Pos);

  /* Configure PLL domain end prescaller */
  MODIFY_REG(RCC->PLLCFGR, (RCC_PLLCFGR_PLLSRC_Msk | RCC_PLLCFGR_PLLM_Msk | RCC_PLLCFGR_PLLN_Msk), (
      RCC_PLLCFGR_PLLSRC_HSE
    | 25 << RCC_PLLCFGR_PLLM_Pos
    | 0 << RCC_PLLCFGR_PLLP_Pos
    | 7 << RCC_PLLCFGR_PLLQ_Pos
    | 336 << RCC_PLLCFGR_PLLN_Pos
  ));

  /* PLL isn't divided */
  MODIFY_REG(RCC->PLLCFGR, RCC_PLLCFGR_PLLP_Msk, 0);
  
  /* PLL enable and wait till it's ready */
  PREG_SET(RCC->CR, RCC_CR_PLLON_Pos);
  while (!(PREG_CHECK(RCC->CR, RCC_CR_PLLRDY_Pos)));


  /* AHB isn't divided */
  // MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, LL_RCC_SYSCLK_DIV_1);
  /* APB1 divided by 4 */
  MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1, RCC_CFGR_PPRE1_DIV4);
  /* APB2 divided by 2 */
  MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2, RCC_CFGR_PPRE2_DIV2);

  /* Enable PLL as sysclock and wait till it's ready */
  MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);
  while(!(READ_BIT(RCC->CFGR, RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL));

  SystemCoreClock = 168000000U;
  RccClocks.HCLK_Freq = SystemCoreClock;
  RccClocks.PCLK1_Freq = 42000000U;
  RccClocks.PCLK1_Freq_Tim = 84000000U;
  RccClocks.PCLK2_Freq = 84000000U;
  RccClocks.PCLK2_Freq_Tim = 168000000U;




  /*****************************************************************************************/
  /*****************************************************************************************/
  /*****************************************************************************************/
  /* Freeze some peripherals for the debug purpose */
  #ifdef DEBUG
  SET_BIT(DBGMCU->APB1FZ, (
      DBGMCU_APB1_FZ_DBG_TIM7_STOP
    | DBGMCU_APB1_FZ_DBG_IWDG_STOP
    | DBGMCU_APB1_FZ_DBG_WWDG_STOP
  ));
  #endif

  /*****************************************************************************************/
  /* IWDG */
  WRITE_REG(IWDG->KR, IWDG_KEY_ENABLE);
  WRITE_REG(IWDG->KR, IWDG_KEY_WR_ACCESS_ENABLE);
  WRITE_REG(IWDG->PR, IWDG_PR_PR & (IWDG_PR_PR_2 | IWDG_PR_PR_0)); /*!< Divider by 128 */
  WRITE_REG(IWDG->RLR, IWDG_RLR_RL & (625 - 1));
  while (!(PREG_CHECK(IWDG->SR, IWDG_SR_PVU_Pos)));
  WRITE_REG(IWDG->KR, IWDG_KEY_RELOAD);



   /* Peripheral clock */
  /* APB1 */
  SET_BIT(RCC->APB1ENR, (
      RCC_APB1ENR_TIM7EN
    | RCC_APB1ENR_USART2EN
    | RCC_APB1ENR_USART3EN
    // | RCC_APB1ENR_SPI2EN
    // | RCC_APB1ENR_SPI3EN
  ));

  /* AHB1 */
  SET_BIT(RCC->AHB1ENR, ( 
      RCC_AHB1ENR_GPIOAEN
    | RCC_AHB1ENR_GPIOBEN
    | RCC_AHB1ENR_GPIOCEN
    | RCC_AHB1ENR_GPIODEN
    | RCC_AHB1ENR_GPIOEEN
    | RCC_AHB1ENR_GPIOGEN
    | RCC_AHB1ENR_CRCEN
  ));

  /* APB2 */
  SET_BIT(RCC->APB2ENR, (
      RCC_APB2ENR_USART1EN
    // | RCC_APB2ENR_SPI1EN
  ));

  /* AHB3 */
  // SET_BIT(RCC->AHB3ENR, (
  //     RCC_AHB3ENR_FSMCEN
  // ));
  /* Wait for FMC starts */
  // while (!(PREG_CHECK(RCC->AHB3ENR, RCC_AHB3ENR_FSMCEN_Pos)));

}


/*************************** © Zoo IPS, 2021 **********************************/
