/**
  ******************************************************************************
  * File Name          : srv_rs232_receiver.c
  * Description        : This file provides code for FreeRTOS RS232 recevier
  *                      service
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "freertos.h"


// /* Global variables ----------------------------------------------------------*/
// SemaphoreHandle_t xRs232RxIRQ = NULL;
// SemaphoreHandle_t xRs232DataReady = NULL;

// /* Local variables -----------------------------------------------------------*/
// static TaskHandle_t xRs232ReceiveTask = NULL;
// static TaskHandle_t xRs232DataReadyTask = NULL;

// /* Function prototypes */
// static void xRs232ReceiveHandle(void *pvParameters);
// static void xRs232DataReadyHandle(void *pvParameters);





/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

// /**
//   * @brief  Provides FreeRTOS Logger service.
//   * @return None
//   */
// void srvRS232Receiver(void) {
//   xRs232RxIRQ = xSemaphoreCreateBinary();
//   xRs232DataReady = xSemaphoreCreateBinary();
//   xTaskCreate(xRs232ReceiveHandle, "RS232Receive", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1), &xRs232ReceiveTask);
//   xTaskCreate(xRs232DataReadyHandle, "RS232Receive", configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1), &xRs232DataReadyTask);
// }


// /**
//   * @brief  Sends a message to a log representer.
//   * @return None
//   */
// static void xRs232ReceiveHandle(void *pvParameters) {
//   (void)pvParameters;

//   // static portBASE_TYPE xHigherPriorityTaskWoken = NULL; 
//   while(1) {
//     if (xSemaphoreTakeFromISR(xRs232RxIRQ, NULL) == pdTRUE) {
//       // printf("Receive Semaphor\n");
//       RS232_Handler();
//     }
//   }
// }


// /**
//   * @brief  Sends a message to a log representer.
//   * @return None
//   */
// static void xRs232DataReadyHandle(void *pvParameters) {
//   (void)pvParameters;

//   // static portBASE_TYPE xHigherPriorityTaskWoken = NULL; 
//   while(1) {
//     if (xSemaphoreTake(xRs232DataReady, TIMER_RX_SEM_TO_WAIT) == pdTRUE) {
//       static uint8_t buf[16];
//       buf[15] = '\n';
//       RS232_RxBufferRead(buf, 16);
//       printf("%s\n", buf);
//       // printf("Receive Semaphor\n");
//       // RS232_Handler();
//     }
//   }
// }

// /*************************** © Zoo IPS, 2021 **********************************/
