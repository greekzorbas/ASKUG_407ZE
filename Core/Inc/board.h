/**
  ******************************************************************************
  * @file           : board.h
  * @brief          : Board specific definitions
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BOARD_H
#define __BOARD_H

#ifdef __cplusplus
  extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "main.h"



// USART peripheral conncted to RSx
#define USART_1_Port                                GPIOA
#define USART_1_TX_Pin                              GPIO_PIN_9
#define USART_1_TX_Pin_Pos                          GPIO_PIN_9_Pos
#define USART_1_TX_Pin_Mask                         GPIO_PIN_9_Mask
#define USART_1_RX_Pin                              GPIO_PIN_10
#define USART_1_RX_Pin_Pos                          GPIO_PIN_10_Pos
#define USART_1_RX_Pin_Mask                         GPIO_PIN_10_Mask

#define USART_2_Port                                GPIOD
#define USART_2_TX_Pin                              GPIO_PIN_5
#define USART_2_TX_Pin_Pos                          GPIO_PIN_5_Pos
#define USART_2_TX_Pin_Mask                         GPIO_PIN_5_Mask
#define USART_2_RX_Pin                              GPIO_PIN_6
#define USART_2_RX_Pin_Pos                          GPIO_PIN_6_Pos
#define USART_2_RX_Pin_Mask                         GPIO_PIN_6_Mask

#define USART_3_Port                                GPIOB
#define USART_3_TX_Pin                              GPIO_PIN_10
#define USART_3_TX_Pin_Pos                          GPIO_PIN_10_Pos
#define USART_3_TX_Pin_Mask                         GPIO_PIN_10_Mask
#define USART_3_RX_Pin                              GPIO_PIN_11
#define USART_3_RX_Pin_Pos                          GPIO_PIN_11_Pos
#define USART_3_RX_Pin_Mask                         GPIO_PIN_11_Mask

// RSx peripheral to connect                
#define RS232_0                                     USART1
#define RS485_0                                     USART2
#define RS485_1                                     USART3

#define RS485_1_E_Port                              GPIOG
#define RS485_1_E_Pin                               GPIO_PIN_4
#define RS485_1_E_Pin_Pos                           GPIO_PIN_4_Pos
#define RS485_1_E_Pin_Mask                          GPIO_PIN_4_Mask

#define RS485_2_E_Port                              GPIOG
#define RS485_2_E_Pin                               GPIO_PIN_2
#define RS485_2_E_Pin_Pos                           GPIO_PIN_2_Pos
#define RS485_2_E_Pin_Mask                          GPIO_PIN_2_Mask

// Dallas thermo sensor
#define DS18B20_Port                                GPIOE
#define DS18B20_Pin                                 GPIO_PIN_3
#define DS18B20_Pin_Pos                             GPIO_PIN_3_Pos
#define DS18B20_Pin_Mask                            GPIO_PIN_3_Mask

// HS infrared sensor
#define HS0038_Port                                 GPIOF
#define HS0038_Pin                                  GPIO_PIN_2
#define HS0038_Pin_Pos                              GPIO_PIN_2_Pos
#define HS0038_Pin_Mas                              GPIO_PIN_2_Mask

// LED
#define LED_Port                                    GPIOE
#define LED_S1_Pin                                  GPIO_PIN_8
#define LED_S1_Pin_Pos                              GPIO_PIN_8_Pos
#define LED_S1_Pin_Mask                             GPIO_PIN_8_Mask
#define LED_S2_Pin                                  GPIO_PIN_9
#define LED_S2_Pin_Pos                              GPIO_PIN_9_Pos
#define LED_S2_Pin_Mask                             GPIO_PIN_9_Mask
#define LED_S3_Pin                                  GPIO_PIN_10
#define LED_S3_Pin_Pos                              GPIO_PIN_10_Pos
#define LED_S3_Pin_Mask                             GPIO_PIN_10_Mask
#define LED_S4_Pin                                  GPIO_PIN_11
#define LED_S4_Pin_Pos                              GPIO_PIN_11_Pos
#define LED_S4_Pin_Mask                             GPIO_PIN_11_Mask

// Buttons
#define BTN_Port                                    GPIOE
#define BTN_S1_Pin                                  GPIO_PIN_13
#define BTN_S1_Pin_Pos                              GPIO_PIN_13_Pos
#define BTN_S1_Pin_Mask                             GPIO_PIN_13_Mask
#define BTN_S2_Pin                                  GPIO_PIN_12
#define BTN_S2_Pin_Pos                              GPIO_PIN_12_Pos
#define BTN_S2_Pin_Mask                             GPIO_PIN_12_Mask
#define BTN_S3_Pin                                  GPIO_PIN_14
#define BTN_S3_Pin_Pos                              GPIO_PIN_14_Pos
#define BTN_S3_Pin_Mask                             GPIO_PIN_14_Mask
#define BTN_S4_Pin                                  GPIO_PIN_15
#define BTN_S4_Pin_Pos                              GPIO_PIN_15_Pos
#define BTN_S4_Pin_Mask                             GPIO_PIN_15_Mask

// I2C2 that 24C02 Flash is connected to
#define I2C2_Port                                   GPIOF
#define I2C2_SDA_Pin                                GPIO_PIN_0
#define I2C2_SDA_Pin_Pos                            GPIO_PIN_0_Pos
#define I2C2_SDA_Pin_Mask                           GPIO_PIN_0_Mask
#define I2C2_SCL_Pin                                GPIO_PIN_1
#define I2C2_SCL_Pin_Pos                            GPIO_PIN_1_Pos
#define I2C2_SCL_Pin_Mask                           GPIO_PIN_1_Mask

// SPI3 that SST25VF016 Flash is connected to
#define SPI3_Port                                   GPIOB
#define SPI3_MOSI_Pin                               GPIO_PIN_5
#define SPI3_MOSI_Pin_Pos                           GPIO_PIN_5_Pos
#define SPI3_MOSI_Pin_Mask                          GPIO_PIN_5_Mask
#define SPI3_MISO_Pin                               GPIO_PIN_4
#define SPI3_MISO_Pin_Pos                           GPIO_PIN_4_Pos
#define SPI3_MISO_Pin_Mask                          GPIO_PIN_4_Mask
#define SPI3_SCK_Pin                                GPIO_PIN_3
#define SPI3_SCK_Pin_Pos                            GPIO_PIN_3_Pos
#define SPI3_SCK_Pin_Mask                           GPIO_PIN_3_Mask
// ! SPI3 NE has a different GPIO port 
#define SPI3_NE_Port                                GPIOG
#define SPI3_NE_Pin                                 GPIO_PIN_10
#define SPI3_NE_Pin_Pos                             GPIO_PIN_10_Pos
#define SPI3_NE_Pin_Mask                            GPIO_PIN_10_Mask

// CAN
#define CAN1_Port                                   GPIOD
#define CAN1_RX_Pin                                 GPIO_PIN_0
#define CAN1_RX_Pin_Pos                             GPIO_PIN_0_Pos
#define CAN1_RX_Pin_Mask                            GPIO_PIN_0_Mask
#define CAN1_TX_Pin                                 GPIO_PIN_1
#define CAN1_TX_Pin_Pos                             GPIO_PIN_1_Pos
#define CAN1_TX_Pin_Mask                            GPIO_PIN_1_Mask

#define CAN2_Port                                   GPIOB
#define CAN2_RX_Pin                                 GPIO_PIN_12
#define CAN2_RX_Pin_Pos                             GPIO_PIN_12_Pos
#define CAN2_RX_Pin_Mask                            GPIO_PIN_12_Mask
#define CAN2_TX_Pin                                 GPIO_PIN_13
#define CAN2_TX_Pin_Pos                             GPIO_PIN_13_Pos
#define CAN2_TX_Pin_Mask                            GPIO_PIN_13_Mask

// OLED connector
#define OLED_SDA_Port                               GPIOA
#define OLED_SDA_Pin                                GPIO_PIN_0
#define OLED_SDA_Pin_Pos                            GPIO_PIN_0_Pos
#define OLED_SDA_Pin_Mask                           GPIO_PIN_0_Mask

#define OLED_SCL_Port                               GPIOC
#define OLED_SCL_Pin                                GPIO_PIN_3
#define OLED_SCL_Pin_Pos                            GPIO_PIN_3_Pos
#define OLED_SCL_Pin_Mask                           GPIO_PIN_3_Mask

// SDIO
#define SDIO_Port                                   GPIOC
#define SDIO_D0_Pin                                 GPIO_PIN_8
#define SDIO_D0_Pin_Pos                             GPIO_PIN_8_Pos
#define SDIO_D0_Pin_Mask                            GPIO_PIN_8_Mask
#define SDIO_D1_Pin                                 GPIO_PIN_9
#define SDIO_D1_Pin_Pos                             GPIO_PIN_9_Pos
#define SDIO_D1_Pin_Mask                            GPIO_PIN_9_Mask
#define SDIO_D2_Pin                                 GPIO_PIN_10
#define SDIO_D2_Pin_Pos                             GPIO_PIN_10_Pos
#define SDIO_D2_Pin_Mask                            GPIO_PIN_10_Mask
#define SDIO_D3_Pin                                 GPIO_PIN_11
#define SDIO_D3_Pin_Pos                             GPIO_PIN_11_Pos
#define SDIO_D3_Pin_Mask                            GPIO_PIN_11_Mask
#define SDIO_SCK_Pin                                GPIO_PIN_12
#define SDIO_SCK_Pin_Pos                            GPIO_PIN_12_Pos
#define SDIO_SCK_Pin_Mask                           GPIO_PIN_12_Mask
// SDIO CMD has a different GPIO port
#define SDIO_CMD_Port                               GPIOD
#define SDIO_CMD_Pin                                GPIO_PIN_2
#define SDIO_CMD_Pin_Pos                            GPIO_PIN_2_Pos
#define SDIO_CMD_Pin_Mask                           GPIO_PIN_2_Mask

// USB OTG
#define USG_OTG_FS_Port                             GPIOA
#define USG_OTG_FS_DM_Pin                           GPIO_PIN_11
#define USG_OTG_FS_DM_Pin_Pos                       GPIO_PIN_11_Pos
#define USG_OTG_FS_DM_Pin_Mask                      GPIO_PIN_11_Mask
#define USG_OTG_FS_DP_Pin                           GPIO_PIN_12
#define USG_OTG_FS_DP_Pin_Pos                       GPIO_PIN_12_Pos
#define USG_OTG_FS_DP_Pin_Mask                      GPIO_PIN_12_Mask

#define USG_OTG_HS_Port                             GPIOB
#define USG_OTG_HS_DM_Pin                           GPIO_PIN_14
#define USG_OTG_HS_DM_Pin_Pos                       GPIO_PIN_14_Pos
#define USG_OTG_HS_DM_Pin_Mask                      GPIO_PIN_14_Mask
#define USG_OTG_HS_DP_Pin                           GPIO_PIN_15
#define USG_OTG_HS_DP_Pin_Pos                       GPIO_PIN_15_Pos
#define USG_OTG_HS_DP_Pin_Mask                      GPIO_PIN_15_Mask

// ETH PHY pins connected to different GPIO ports
#define ETH_MDC_Port                                GPIOC
#define ETH_MDC_Pin                                 GPIO_PIN_1
#define ETH_MDC_Pin_Pos                             GPIO_PIN_1_Pos
#define ETH_MDC_Pin_Mask                            GPIO_PIN_1_Mask

#define ETH_REF_CLK_Port                            GPIOA
#define ETH_REF_CLK_Pin                             GPIO_PIN_1
#define ETH_REF_CLK_Pin_Pos                         GPIO_PIN_1_Pos
#define ETH_REF_CLK_Pin_Mask                        GPIO_PIN_1_Mask

#define ETH_MDIO_Port                               GPIOA
#define ETH_MDIO_Pin                                GPIO_PIN_2
#define ETH_MDIO_Pin_Pos                            GPIO_PIN_2_Pos
#define ETH_MDIO_Pin_Mask                           GPIO_PIN_2_Mask

#define ETH_CRS_DV_Port                             GPIOA
#define ETH_CRS_DV_Pin                              GPIO_PIN_7
#define ETH_CRS_DV_Pin_Pos                          GPIO_PIN_7_Pos
#define ETH_CRS_DV_Pin_Mask                         GPIO_PIN_7_Mask

#define ETH_RXD0_Port                               GPIOC
#define ETH_RXD0_Pin                                GPIO_PIN_4
#define ETH_RXD0_Pin_Pos                            GPIO_PIN_4_Pos
#define ETH_RXD0_Pin_Mask                           GPIO_PIN_4_Mask

#define ETH_RXD1_Port                               GPIOC
#define ETH_RXD1_Pin                                GPIO_PIN_5
#define ETH_RXD1_Pin_Pos                            GPIO_PIN_5_Pos
#define ETH_RXD1_Pin_Mask                           GPIO_PIN_5_Mask

#define ETH_TX_EN_Port                              GPIOG
#define ETH_TX_EN_Pin                               GPIO_PIN_11
#define ETH_TX_EN_Pin_Pos                           GPIO_PIN_11_Pos
#define ETH_TX_EN_Pin_Mask                          GPIO_PIN_11_Mask

#define ETH_TXD0_Port                               GPIOG
#define ETH_TXD0_Pin                                GPIO_PIN_13
#define ETH_TXD0_Pin_Pos                            GPIO_PIN_13_Pos
#define ETH_TXD0_Pin_Mask                           GPIO_PIN_13_Mask

#define ETH_TXD1_Port                               GPIOG
#define ETH_TXD1_Pin                                GPIO_PIN_14
#define ETH_TXD1_Pin_Pos                            GPIO_PIN_14_Pos
#define ETH_TXD1_Pin_Mask                           GPIO_PIN_14_Mask




#ifdef __cplusplus
  }
#endif
#endif /* __BOARD_H */


/*************************** © Zoo IPS, 2021 **********************************/
