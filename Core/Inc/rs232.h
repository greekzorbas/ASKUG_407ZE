/**
  ******************************************************************************
  * File Name          : rs232.h
  * Description        : This file provides a header for rs232.c file that
  *                      defines RS232 operations.
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef __RS232_H
#define __RS232_H

#ifdef __cplusplus
  extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/* Global variables ----------------------------------------------------------*/

/* Private defines -----------------------------------------------------------*/
#define RS232_BAUDRATE              115200U
#define RS232_BUF_LEN               (MIN_BUF_LEN << 1) // 128b
#define RS232_BUF_MASK              (RS232_BUF_LEN - 1)

#define TIMER_RX_SEM_TO_WAIT        pdMS_TO_TICKS(10UL)


/* Private macros ------------------------------------------------------------*/
#define RS232_BRR_Mantissa          (uint16_t)((RccClocks.PCLK2_Freq / (RS232_BAUDRATE * 16)) << 4)
#define RS232_BRR_Fractal           (uint16_t)((((RccClocks.PCLK2_Freq) / (RS232_BAUDRATE * 0.16)) - ((RccClocks.PCLK2_Freq / (RS232_BAUDRATE * 16)) * 100)) * 16 / 100)
#define RS232_BRR_Value             (uint16_t)((RS232_BRR_Mantissa & 0xfff0) | (RS232_BRR_Fractal & 0x000f))


/* Exported functions prototypes ---------------------------------------------*/
void RS232_Handler(void);
uint8_t RS232_RxBufferRead(uint8_t *buf, uint16_t len);
/* FreeRTOS services */
void srvRS232Receiver(void);
/* Redefine IRQ handler */
#define RS232_IRQHandler            USART1_IRQHandler


#ifdef __cplusplus
  }
#endif
#endif /*__RS232_H */


/*************************** © Zoo IPS, 2021 **********************************/
