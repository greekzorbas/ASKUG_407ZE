/**
  ******************************************************************************
  * @file    stm32f4xx_it.h
  * @brief   This file contains the headers of the interrupt handlers.
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F4xx_IT_H
#define __STM32F4xx_IT_H

#ifdef __cplusplus
  extern "C" {
#endif 

/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
// void SVC_Handler(void); // Redefined in FreeRTOSCongif.h
void DebugMon_Handler(void);
// void PendSV_Handler(void); // Redefined in FreeRTOSCongif.h
// void SysTick_Handler(void); // Redefined in FreeRTOSCongif.h
void RCC_IRQHandler(void);
void EXTI0_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
// void USART1_IRQHandler(void); // Redefined in rs232.h
// void USART2_IRQHandler(void); // Redefined in rs485.h
// void USART3_IRQHandler(void); // Redefined in rs485.h
void RTC_Alarm_IRQHandler(void);
void TIM7_IRQHandler(void);


#ifdef __cplusplus
  }
#endif

#endif /* __STM32F4xx_IT_H */


/*************************** © Zoo IPS, 2021 **********************************/
