/**
  ******************************************************************************
  * File Name          : rs485.h
  * Description        : This file provides a header for rs485.c file that
  *                      defines RS485 operations.
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef __RS485_H
#define __RS485_H

#ifdef __cplusplus
  extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/* Global variables ----------------------------------------------------------*/

/* Private defines -----------------------------------------------------------*/
// typedef enum {
//   item_1  = 0,
//   item_2  = 1 
// } RS485_TypeDef;

typedef struct {
  USART_TypeDef*      usart;
  uint8_t*            rxBuf;
  uint16_t            rxBufLen;
  uint16_t            rxBufMask;
  uint8_t             rxBufPrtIn;
  uint8_t             rxBufPrtOut;
  uint8_t*            txBuf;
  uint16_t            txBufLen;
  uint16_t            txBufMask;
  uint8_t             txBufPrtIn;
  uint8_t             txBufPrtOut;
  SemaphoreHandle_t*  irqSem;
  SemaphoreHandle_t*  dataReadySem;
} RS485_TypeDef;

#define RS485_1_BAUDRATE              115200U
#define RS485_1_BUF_LEN               (MIN_BUF_LEN << 2) // 256b
#define RS485_1_BUF_MASK              (RS485_1_BUF_LEN - 1)

#define RS485_2_BAUDRATE              115200U
#define RS485_2_BUF_LEN               (MIN_BUF_LEN << 4) // 1Kb
#define RS485_2_BUF_MASK              (RS485_2_BUF_LEN - 1)

/* Private macros ------------------------------------------------------------*/
#define RS485_1_BRR_Mantissa          (uint16_t)((RccClocks.PCLK1_Freq / (RS485_1_BAUDRATE * 16)) << 4)
#define RS485_1_BRR_Fractal           (uint16_t)((((RccClocks.PCLK1_Freq) / (RS485_1_BAUDRATE * 0.16)) - ((RccClocks.PCLK1_Freq / (RS485_1_BAUDRATE * 16)) * 100)) * 16 / 100)
#define RS485_1_BRR_Value             (uint16_t)((RS485_1_BRR_Mantissa & 0xfff0) | (RS485_1_BRR_Fractal & 0x000f))

#define RS485_2_BRR_Mantissa          (uint16_t)((RccClocks.PCLK1_Freq / (RS485_2_BAUDRATE * 16)) << 4)
#define RS485_2_BRR_Fractal           (uint16_t)((((RccClocks.PCLK1_Freq) / (RS485_2_BAUDRATE * 0.16)) - ((RccClocks.PCLK1_Freq / (RS485_2_BAUDRATE * 16)) * 100)) * 16 / 100)
#define RS485_2_BRR_Value             (uint16_t)((RS485_1_BRR_Mantissa & 0xfff0) | (RS485_2_BRR_Fractal & 0x000f))

#define RS485_1_TX                    PIN_H(RS485_1_E_Port, RS485_1_E_Pin_Pos)
#define RS485_1_RX                    PIN_L(RS485_1_E_Port, RS485_1_E_Pin_Pos)

#define RS485_2_TX                    PIN_H(RS485_2_E_Port, RS485_2_E_Pin_Pos)
#define RS485_2_RX                    PIN_L(RS485_2_E_Port, RS485_2_E_Pin_Pos)

/* Exported functions prototypes ---------------------------------------------*/
void RS485_ReceiveHandler(RS485_TypeDef* item);
uint8_t RS485_RxBufferRead(RS485_TypeDef* item, uint8_t *buf, uint16_t len);
/* FreeRTOS services */
void srvRS485Receiver(void);

// redefine IRQ handler
#define RS485_1_IRQHandler            USART2_IRQHandler
#define RS485_2_IRQHandler            USART3_IRQHandler



#ifdef __cplusplus
  }
#endif
#endif /*__RS485_H */


/*************************** © Zoo IPS, 2021 **********************************/
