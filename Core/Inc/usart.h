/**
  ******************************************************************************
  * File Name          : usart.h
  * Description        : This file provides a header for usars.c file that
  *                      defines USART instances.
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */

#ifndef __USART_H
#define __USART_H

#ifdef __cplusplus
  extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/* Global variables ----------------------------------------------------------*/

/* Private defines -----------------------------------------------------------*/
#define MIN_BUF_LEN           64U
#define MIN_BUF_MASK          (MIN_BUF_LEN - 1U)

/* Private macros ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
void USART1_Init(void);
int8_t USART_Init(USART_TypeDef* dev, uint16_t baudrate);




#ifdef __cplusplus
  }
#endif
#endif /*__USART_H */


/*************************** © Zoo IPS, 2021 **********************************/
